const express = require('express');
const morgan = require('morgan');

const app = express();
app.use(morgan());

app.get('/', (req, res) => {
    res.json({ service: 'js-backend' });
});

const port = 8082;
app.listen(port, () => {
    console.log(`App Listening on port ${port}.`);
});